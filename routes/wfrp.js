var express = require('express');
var router = express.Router();
var wfrp = require('../src/models/wfrp');


router.get('/',function(req,res) {
    wfrp.get(res, req);
});
router.get('/:id',function(req,res) {
    wfrp.getByID(req.params.id,res, req);
});

router.get('/author/:id',function(req,res) {
    wfrp.getByAuthor(req.params.id,res, req);
});

router.post('/',function(req,res) {
    console.log(req);
    wfrp.create(req.body,res, req);
});
router.put('/:id',function(req,res) {
    wfrp.update(req.body,req.params.id,res, req);
});
router.delete('/:id',function(req,res) {
    wfrp.delete(req.params.id,res, req);
});

module.exports = router;
