var connection = require ('../../connection');
connection.init();

function Wfrp() {
    this.get = function(res, req) {
        connection.acquire(function(err,con) {
            con.query('select * from wfrp', function(err,result) {
                con.release();
                res.send(result);
                console.log("Get successful");
            });
        });
    };
    this.getByID = function(id,res, req) {
        connection.acquire(function(err,con) {
            con.query('select * from wfrp where id = ?', id, function(err,result) {
                con.release();
                res.send(result);
                console.log("Get by ID successful");
            });
        });
    };

    this.getByAuthor = function(id,res, req) {
        connection.acquire(function(err,con) {
            con.query('select * from wfrp where author = ?', id, function(err,result) {
                con.release();
                res.send(result);
                console.log("Get by ID successful");
            });
        });
    };

    this.create = function(wfrp,res, req) {
        connection.acquire(function(err,con) {
            console.log(wfrp);
            con.query('insert into wfrp set ?', wfrp, function(err,result) {
                con.release();
                if (err) {
                    console.log(err);
                    res.send({status:1, message:'Character creation fail'});
                } else {
                    res.send({status:0, message:'Character create success'});
                    console.log("Post successful");
                }
            });
        });
    };
    this.update = function(wfrp,id,res,req) {
        connection.acquire(function(err,con) {
            con.query('update wfrp set ? where id = ?', [wfrp, id], function(err,result) {
                con.release();
                if (err) {
                    res.send({status:1, message:'Character update fail'});
                } else {
                    res.send({status:0, message:'Character update success'});
                    console.log("Put successful");
                }
            });
        });
    };
    this.delete = function(id,res, req) {
        connection.acquire(function(err,con) {
            con.query('delete from wfrp where id = ?', id, function(err,result) {
                con.release();
                if (err) {
                    res.send({status:1, message:'Character delete fail'});
                } else {
                    res.send({status:0, message:'Character delete success'});
                    console.log("Delete successful");
                }
            });
        });
    };
};

module.exports = new Wfrp();
